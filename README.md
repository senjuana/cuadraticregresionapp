![Image](http://i.imgur.com/8AWyo3f.png)


# Cuadratic Regresion App


## About
Este es un repositorio publico para poder guardar la app de Cuadratic Regresion de la materia de Moviles.

## Requirements
Supported operating systems:
* Development enviroments: GNU/Linux, MacOS X, Windows.

Requirements:
* Androdid SDK
* androidplot-core:1.5.6

## Getting the code
	$ git clone https://gitlab.com/senjuana/cuadraticregresionapp.git
	$ cd cuadraticregresionapp


# Contributing
Este codigo sigue nuestra version de Contributor Covenant antes de hacer aportes al codigo lee con atencion las reglas.
[Contributing](https://gitlab.com/senjuana/cuadraticregresionapp/blob/master/CONTRIBUTING.md)

# FAQ

* **Cual es el punto de compartir tu codigo?**

    Mi unico interes es el de poder compartir mi codigo como  herramienta de aprendizaje.

* **Puedo utilizar este codigo en mis proyectos?**

    Siempre y cuando sigas los parametros de la licencia cualquiera puede utilizar o modiificar las implementaciones de este repositorio para cualquier proyecto personal.



