package com.example.crapp;

public class Cregretion {
    //variables publicas
    public float [ ] datax = new float[100] ;
    public float [ ] datay = new float[100] ;
    public int n;

    //variables internas
    private float [][] detma = new float[3][3];
    private float [][] detmb = new float[3][3];
    private float [][] detmc = new float[3][3];
    private float [][] detms = new float[3][3];
    private float sx,sy,sx2,sx3,sx4,sxy,sx2y;
    private float deta,detb,detc,dets;

    //Constructor
    public Cregretion(float[] datax, float[] datay, int n) {
        this.datax = datax;
        this.datay = datay;
        this.n = n;
    }

    //codigo

    public float predict(float x){
        clearall();
        fillsumas();
        filldetm();
        filldet();
        float a,b,c;

        a = deta/dets;
        b = detb/dets;
        c = detc/dets;

        return ((a*(x*x))+(b*x)+(c));
    }

    public String formu(){
        clearall();
        fillsumas();
        filldetm();
        filldet();
        float a,b,c;
        String formu;

        a = deta/dets;
        b = detb/dets;
        c = detc/dets;
        formu = "Y = ("+Float.toString(a)+")X^2 +("+Float.toString(b)+")X + ("+Float.toString(c)+") ";
        return formu;
    }


    private float matrixDeterminant (float[][] matrix) {
        float temporary[][];
        float result = 0;

        if (matrix.length == 1) {
            result = matrix[0][0];
            return (result);
        }

        if (matrix.length == 2) {
            result = ((matrix[0][0] * matrix[1][1]) - (matrix[0][1] * matrix[1][0]));
            return (result);
        }

        for (int i = 0; i < matrix[0].length; i++) {
            temporary = new float[matrix.length - 1][matrix[0].length - 1];

            for (int j = 1; j < matrix.length; j++) {
                for (int k = 0; k < matrix[0].length; k++) {
                    if (k < i) {
                        temporary[j - 1][k] = matrix[j][k];
                    } else if (k > i) {
                        temporary[j - 1][k - 1] = matrix[j][k];
                    }
                }
            }

            result += matrix[0][i] * Math.pow (-1, (float) i) * matrixDeterminant (temporary);
        }
        return (result);
    }

    private void filldet(){
        deta = matrixDeterminant(detma);
        detb = matrixDeterminant(detmb);
        detc = matrixDeterminant(detmc);
        dets = matrixDeterminant(detms);
    }

    private void fillsumas(){
        for (int i = 0;i<n;i++) {
            sx   += datax[i];
            sx2  += datax[i] * datax[i];
            sx3  += datax[i] * datax[i] * datax[i];
            sx4  += datax[i] * datax[i] * datax[i] * datax[i];
            sy   += datay[i];
            sx2y += ((datax[i] * datax[i]) * datay[i]);
            sxy  += datax[i] * datay[i];
        }
    }

    private void filldetm(){
        //dete a
        detma[0][0] = sy;
        detma[0][1] = sx;
        detma[0][2] = n;
        detma[1][0] = sxy;
        detma[1][1] = sx2;
        detma[1][2] = sx;
        detma[2][0] = sx2y;
        detma[2][1] = sx3;
        detma[2][2] = sx2;
        //dete b
        detmb[0][0] = sx2;
        detmb[0][1] = sy;
        detmb[0][2] = n;
        detmb[1][0] = sx3;
        detmb[1][1] = sxy;
        detmb[1][2] = sx;
        detmb[2][0] = sx4;
        detmb[2][1] = sx2y;
        detmb[2][2] = sx2;
        //dete c
        detmc[0][0] = sx2;
        detmc[0][1] = sx;
        detmc[0][2] = sy;
        detmc[1][0] = sx3;
        detmc[1][1] = sx2;
        detmc[1][2] = sxy;
        detmc[2][0] = sx4;
        detmc[2][1] = sx3;
        detmc[2][2] = sx2y;
        //dete s
        detms[0][0] = sx2;
        detms[0][1] = sx;
        detms[0][2] = n;
        detms[1][0] = sx3;
        detms[1][1] = sx2;
        detms[1][2] = sx;
        detms[2][0] = sx4;
        detms[2][1] = sx3;
        detms[2][2] = sx2;
    }


    private void clearall(){
        //clear the sums
        sx   = 0;
        sx2  = 0;
        sx3  = 0;
        sx4  = 0;
        sy   = 0;
        sx2y = 0;
        sxy  = 0;
        //clear the det matrix
        for (int i = 0;i<3;i++){
            for(int j = 0;j<3;j++){
                detma[i][j] = 0;
                detmb[i][j] = 0;
                detmc[i][j] = 0;
                detms[i][j] = 0;
            }
        }
    }



    //getters and setters
    public float[] getDatax() {
        return datax;
    }

    public void setDatax(float[] datax) {
        this.datax = datax;
    }

    public float[] getDatay() {
        return datay;
    }

    public void setDatay(float[] datay) {
        this.datay = datay;
    }

    public int getN() {
        return n;
    }

    public void setN(int n) {
        this.n = n;
    }
}
