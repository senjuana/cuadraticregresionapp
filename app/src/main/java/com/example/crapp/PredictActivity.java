package com.example.crapp;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.content.Intent;

public class PredictActivity extends AppCompatActivity {

    //variables de entorno
    public float [ ] datax = new float[100] ;
    public float [ ] datay = new float[100] ;
    public int n;
    Cregretion regresion;
    TextView display;
    TextView display2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_predict);
        Intent intent = getIntent();
        //get date of the past activity
        datax = intent.getFloatArrayExtra("x_values");
        datay = intent.getFloatArrayExtra("y_values");
        n = intent.getIntExtra("n_value",100 );

        display = findViewById(R.id.display);
        display2 = findViewById(R.id.display2);
        regresion = new Cregretion(datax,datay,n);


    }


    public void botonclick(View v){
        EditText text01 = findViewById(R.id.text01);
        try{
            float numero = Float.parseFloat(text01.getText().toString());
            float resultado = regresion.predict(numero);
            String formula = regresion.formu();

            display.setText("Prediccion de Y: "+Float.toString(resultado));
            display2.setText(formula);
        }catch (NumberFormatException e){}

    }
}
