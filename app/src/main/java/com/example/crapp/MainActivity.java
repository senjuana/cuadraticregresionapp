package com.example.crapp;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;
import android.content.Intent;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    //variable de entorno
    public float [ ] datax = new float[100] ;
    public float [ ] datay = new float[100] ;
    public int i;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //variable de entorno
        //int i = 0;
        for (int f= 0; f<100;f++){
            datax[f] = -1;
            datay[f] = -1;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action        EditText text01 = findViewById(R.id.);

        } else if (id == R.id.nav_gallery) {
            //ploteo
            Intent myIntent = new Intent(this, PlotActivity.class);
            myIntent.putExtra("x_values", datax);
            myIntent.putExtra("y_values", datay);
            myIntent.putExtra("n_value", i);
            this.startActivity(myIntent);

        } else if (id == R.id.nav_slideshow) {
            //prediccion
            Intent myIntent = new Intent(this, PredictActivity.class);
            myIntent.putExtra("x_values", datax);
            myIntent.putExtra("y_values", datay);
            myIntent.putExtra("n_value", i);
            this.startActivity(myIntent);

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }



    public void botonclick(View v) {
        //textviews
        EditText text01 = findViewById(R.id.coorx);
        EditText text02 = findViewById(R.id.coory);
        TextView display = findViewById(R.id.display);
        try{
            float numero = Float.parseFloat(text01.getText().toString());
            float numero2 = Float.parseFloat(text02.getText().toString());

            if(i == 100){ // revision de que no son mas de 100 puntos
                display.setText("Fueron Ingresados Los puntos necesarios");
            }else{ //guardado de los datos y muestra del registro
                datax[i]= numero;
                datay[i]= numero2;
                display.setText("Ingresado el punto numero: "+(++i));
            }
        }catch (NumberFormatException e){}


    }













}
